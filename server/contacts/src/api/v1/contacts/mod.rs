mod post;
mod get;
mod delete;
mod put;


pub mod id;
pub use post::post;
pub use get::get;
pub use delete::delete;
pub use put::put;
