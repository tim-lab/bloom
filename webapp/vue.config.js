module.exports = {
  pwa: {
    name: 'Bloom',
    iconPaths: {
      favicon32: 'imgs/logos/bloom_64.png',
      favicon32: 'imgs/logos/bloom_64.png',
      favicon16: 'imgs/logos/bloom_32.png',
      appleTouchIcon: 'imgs/logos/bloom_256.png',
      maskIcon: 'imgs/logos/bloom_256.png',
      msTileImage: 'imgs/logos/bloom_256.png',
    },
  },
};
